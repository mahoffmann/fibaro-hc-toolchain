#!/bin/bash
# toolchainInstall

set -e

if [ "$#" -ne 1 ]
then
  echo "#Err: Illegal number of parameters !"
  echo "Type the correct destination path to install toolchain"
  exit
fi

destinationPath=$1

if ! [ -d $destinationPath ]
then
  echo "#Err: This path does not exist !"
  echo "Type the correct destination path to install toolchain"
  exit
fi

if [ ! -d "$destinationPath/toolchain" ]
then
  mkdir "$destinationPath/toolchain"
fi

cp -r "hc2/" "$destinationPath/toolchain/"

tagToReplace="_TAG"
tempPath=$(echo $destinationPath/toolchain | sed -e 's/\//\\\//g')

if [ -w "$destinationPath/toolchain/hc2/usr/i686-hc2br-linux-gnu.toolchain" ] && [ -r "$destinationPath/toolchain/hc2/usr/i686-hc2br-linux-gnu.toolchain" ]
then
  sed -Ei "s/$tagToReplace/$tempPath/g" "$destinationPath/toolchain/hc2/usr/i686-hc2br-linux-gnu.toolchain"
else
  echo "#Err: Incorrect path to file *.toolchain !"
  echo "*.toolchain does not exist in this destination path"
  exit
fi

if [ -w "$destinationPath/toolchain/hc2/usr/environment.sh" ] && [ -r "$destinationPath/toolchain/hc2/usr/environment.sh" ]
then
  sed -Ei "s/$tagToReplace/$tempPath/g" "$destinationPath/toolchain/hc2/usr/environment.sh"
else
  echo "#Err: Incorrect path to file environment.sh !"
  echo "environment.sh does not exist in this destination path"
  exit
fi


exit