# Fibaro toolchain for new-hc and hclite
To install toolchain call:
```sh
./hc2ToolchainInstall.sh <destination-path-to-install-toolchain>
```

To build fibaro-hc repository with new-hc toolchain issue a commands:
```sh
. <path-to-toolchain>/hc2/usr/environment.sh
cmake -DCMAKE_BUILD_TYPE=Debug -DCMAKE_TOOLCHAIN_FILE=<path-to-toolchain>/hc2/usr/i686-hc2br-linux-gnu.toolchain <path-to-source-repo> 
```

Remember to always build HCServer in the same bash where you called:
```sh
./hc2ToolchainInstall.sh <destination-path-to-install-toolchain>
```
