#----------------------------------------------------------------
# Generated CMake target import file for configuration "Release".
#----------------------------------------------------------------

# Commands may need to know the format version.
set(CMAKE_IMPORT_FILE_VERSION 1)

# Import target "Variant" for configuration "Release"
set_property(TARGET Variant APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(Variant PROPERTIES
  IMPORTED_LOCATION_RELEASE "/usr/lib/libVariant.so.1.0.1"
  IMPORTED_SONAME_RELEASE "libVariant.so.1.0.1"
  )

list(APPEND _IMPORT_CHECK_TARGETS Variant )
list(APPEND _IMPORT_CHECK_FILES_FOR_Variant "/usr/lib/libVariant.so.1.0.1" )

# Import target "varsh" for configuration "Release"
set_property(TARGET varsh APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(varsh PROPERTIES
  IMPORTED_LOCATION_RELEASE "/usr/bin/varsh"
  )

list(APPEND _IMPORT_CHECK_TARGETS varsh )
list(APPEND _IMPORT_CHECK_FILES_FOR_varsh "/usr/bin/varsh" )

# Commands beyond this point should not need to know the version.
set(CMAKE_IMPORT_FILE_VERSION)
