rocks_trees = {
   { name = [[user]], root = home..[[/.luarocks]] },
   { name = [[system]], root = [[/opt/atlassian/bamboo-agent/xml-data/build-dir/NHC2-BUIL-JOB1/output/host/usr]] }
}
-- BR cross-compilation
variables = {
   LUA_INCDIR = [[/opt/atlassian/bamboo-agent/xml-data/build-dir/NHC2-BUIL-JOB1/output/host/usr/i686-hc2br-linux-gnu/sysroot/usr/include]],
   LUA_LIBDIR = [[/opt/atlassian/bamboo-agent/xml-data/build-dir/NHC2-BUIL-JOB1/output/host/usr/i686-hc2br-linux-gnu/sysroot/usr/lib]],
   CC = [[/opt/atlassian/bamboo-agent/xml-data/build-dir/NHC2-BUIL-JOB1/output/host/usr/bin/i686-hc2br-linux-gnu-gcc]],
   LD = [[/opt/atlassian/bamboo-agent/xml-data/build-dir/NHC2-BUIL-JOB1/output/host/usr/bin/i686-hc2br-linux-gnu-gcc]],
   CFLAGS = [[-D_LARGEFILE_SOURCE -D_LARGEFILE64_SOURCE -D_FILE_OFFSET_BITS=64   -Os  -fPIC]],
   LIBFLAG = [[-shared ]],
}
external_deps_dirs = { [[/opt/atlassian/bamboo-agent/xml-data/build-dir/NHC2-BUIL-JOB1/output/host/usr/i686-hc2br-linux-gnu/sysroot/usr]] }
gcc_rpath = false
rocks_trees = { [[/opt/atlassian/bamboo-agent/xml-data/build-dir/NHC2-BUIL-JOB1/output/target/usr]] }
wrap_bin_scripts = false
deps_mode = [[none]]
