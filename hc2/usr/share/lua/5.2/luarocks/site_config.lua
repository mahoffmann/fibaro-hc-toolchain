local site_config = {}
site_config.LUAROCKS_PREFIX=[[/opt/atlassian/bamboo-agent/xml-data/build-dir/NHC2-BUIL-JOB1/output/host/usr]]
site_config.LUA_INCDIR=[[/opt/atlassian/bamboo-agent/xml-data/build-dir/NHC2-BUIL-JOB1/output/host/usr/include]]
site_config.LUA_LIBDIR=[[/opt/atlassian/bamboo-agent/xml-data/build-dir/NHC2-BUIL-JOB1/output/host/usr/lib]]
site_config.LUA_BINDIR=[[/opt/atlassian/bamboo-agent/xml-data/build-dir/NHC2-BUIL-JOB1/output/host/usr/bin]]
site_config.LUAROCKS_SYSCONFDIR=[[/opt/atlassian/bamboo-agent/xml-data/build-dir/NHC2-BUIL-JOB1/output/host/usr/etc/luarocks]]
site_config.LUAROCKS_ROCKS_TREE=[[/opt/atlassian/bamboo-agent/xml-data/build-dir/NHC2-BUIL-JOB1/output/host/usr]]
site_config.LUAROCKS_ROCKS_SUBDIR=[[/lib/luarocks/rocks]]
site_config.LUA_DIR_SET=true
site_config.LUAROCKS_UNAME_S=[[Linux]]
site_config.LUAROCKS_UNAME_M=[[x86_64]]
site_config.LUAROCKS_DOWNLOADER=[[curl]]
site_config.LUAROCKS_MD5CHECKER=[[md5sum]]
site_config.LUAROCKS_EXTERNAL_DEPS_SUBDIRS={ bin="bin", lib={ "lib", [[lib/x86_64-linux-gnu]] }, include="include" }
site_config.LUAROCKS_RUNTIME_EXTERNAL_DEPS_SUBDIRS={ bin="bin", lib={ "lib", [[lib/x86_64-linux-gnu]] }, include="include" }
return site_config
